﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EquatablePerformsIdentityCheckException : BasedOnIdentityException
    {
        public EquatablePerformsIdentityCheckException(Type type)
            : base(type)
        {
        }

        public override string Message =>
            $"Expected IEquatable<{Type.Name}>.Equals method to perform value check but looks like it performs identity check";
    }
}
