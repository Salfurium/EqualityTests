﻿namespace EqualityTests.Exceptions
{
    using System;

    public class UnexpectedResultException : TestCaseException
    {
        public UnexpectedResultException(Type type, EqualityTestCase testCase)
            : base(type, testCase)
        {
        }
    }
}
