﻿namespace EqualityTests.Exceptions
{
    using System;

    public class InequalityOperatorValueCheckException : Exception
    {
        public InequalityOperatorValueCheckException(string message) : base(message)
        {
        }
    }
}
