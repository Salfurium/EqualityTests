﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualsValueBasedOnIdentityException : BasedOnIdentityException
    {
        public EqualsValueBasedOnIdentityException(Type type) : base(type)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} to perform value check but looks like it performs identity check";
    }
}
