﻿namespace EqualityTests.Exceptions
{
    using System;

    public class BasedOnIdentityException : TypedException
    {
        public BasedOnIdentityException(Type type)
            : base(type)
        {
        }
    }
}
