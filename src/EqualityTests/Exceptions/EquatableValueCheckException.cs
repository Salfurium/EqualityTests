﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EquatableValueCheckException : UnexpectedResultException
    {
        public EquatableValueCheckException(Type type, EqualityTestCase testCase)
            : base(type, testCase)
        {
        }

        public override string Message =>
            $"Expected IEquatable<{Type.Name}>.Equals method return {TestCase.ExpectedResult}, but {!TestCase.ExpectedResult} was returned for {TestCase.FirstInstance} {TestCase.SecondInstance}";
    }
}
