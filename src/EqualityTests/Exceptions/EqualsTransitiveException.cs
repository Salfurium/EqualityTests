﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualsTransitiveException : Exception
    {
        public EqualsTransitiveException(string message) : base(message)
        {
        }
    }
}
