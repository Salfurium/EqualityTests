﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualityOperatorPerformsIdentityCheckException : BasedOnIdentityException
    {
        public EqualityOperatorPerformsIdentityCheckException(Type type)
        : base(type)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} == operator to perform value check but looks like it performs identity check";
    }
}
