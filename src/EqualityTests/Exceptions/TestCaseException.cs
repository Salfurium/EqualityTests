﻿namespace EqualityTests.Exceptions
{
    using System;
    using System.Collections;

    public abstract class TestCaseException : TypedException
    {
        protected EqualityTestCase TestCase { get; }

        protected TestCaseException(Type type, EqualityTestCase testCase)
            : base(type)
        {
            TestCase = testCase;
        }

        public override IDictionary Data
        {
            get
            {
                var data = base.Data;
                data[nameof(TestCase)] = TestCase;
                return data;
            }
        }
    }
}
