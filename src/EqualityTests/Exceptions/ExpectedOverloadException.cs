﻿namespace EqualityTests.Exceptions
{
    using System;

    public class ExpectedOverloadException : TypedException
    {
        public ExpectedOverloadException(Type type)
            : base(type)
        {
        }
    }
}
