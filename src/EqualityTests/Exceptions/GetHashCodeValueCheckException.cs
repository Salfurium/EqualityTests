﻿namespace EqualityTests.Exceptions
{
    using System;

    public class GetHashCodeValueCheckException : UnexpectedResultException
    {
        public GetHashCodeValueCheckException(Type type, EqualityTestCase testCase)
            : base(type, testCase)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} GetHashCode to return {(TestCase.ExpectedResult ? "equal" : "not equal")} hash codes for {TestCase.FirstInstance} and {TestCase.SecondInstance}";
    }
}
