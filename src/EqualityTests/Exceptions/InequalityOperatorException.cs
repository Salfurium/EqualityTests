﻿namespace EqualityTests.Exceptions
{
    using System;

    public class InequalityOperatorException : Exception
    {
        public InequalityOperatorException(string message) : base(message)
        {
        }
    }
}
