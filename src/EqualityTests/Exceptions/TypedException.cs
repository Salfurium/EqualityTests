﻿namespace EqualityTests.Exceptions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;

    public abstract class TypedException : Exception
    {
        protected Type Type { get; }

        protected TypedException(Type type)
        {
            Type = type;
        }

        public override IDictionary Data => new Dictionary<string, object> { { nameof(Type), Type } };
    }
}
