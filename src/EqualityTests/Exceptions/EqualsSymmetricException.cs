﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualsSymmetricException : UnexpectedResultException
    {
        public EqualsSymmetricException(Type type, string message)
            : base(type, null)
        {
            Message = message;
        }

        public override string Message { get; }
    }
}
