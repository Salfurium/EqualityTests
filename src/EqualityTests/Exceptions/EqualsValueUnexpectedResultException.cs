﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualsValueUnexpectedResultException : UnexpectedResultException
    {
        public EqualsValueUnexpectedResultException(Type type, EqualityTestCase testCase)
        : base(type, testCase)
        {
        }

        public override string Message =>
            $"Expected {TestCase.FirstInstance} to be not equal to {TestCase.SecondInstance}";
    }
}
