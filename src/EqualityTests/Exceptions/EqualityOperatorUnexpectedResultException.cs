﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualityOperatorUnexpectedResultException : UnexpectedResultException
    {
        public EqualityOperatorUnexpectedResultException(Type type, EqualityTestCase testCase)
        : base(type, testCase)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} == operator to returns result {TestCase.ExpectedResult} for {TestCase.FirstInstance} == {TestCase.SecondInstance}";
    }
}
