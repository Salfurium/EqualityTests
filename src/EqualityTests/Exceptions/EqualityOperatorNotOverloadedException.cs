﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EqualityOperatorNotOverloadedException : ExpectedOverloadException
    {
        public EqualityOperatorNotOverloadedException(Type type)
        : base(type)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} to overload == operator with parameters of type {Type.Name}";
    }
}
