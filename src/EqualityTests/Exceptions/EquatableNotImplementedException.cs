﻿namespace EqualityTests.Exceptions
{
    using System;

    public class EquatableNotImplementedException : TypedException
    {
        public EquatableNotImplementedException(Type type)
            : base(type)
        {
        }

        public override string Message => $"Expected type {Type.Name} to implement IEquatable<{Type.Name}>";
    }
}
