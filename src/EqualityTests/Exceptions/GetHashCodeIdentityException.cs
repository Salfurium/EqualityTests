﻿namespace EqualityTests.Assertions
{
    using Exceptions;
    using System;

    public class GetHashCodeIdentityException : BasedOnIdentityException
    {
        public GetHashCodeIdentityException(Type type)
            : base(type)
        {
        }

        public override string Message =>
            $"Expected type {Type.Name} GetHashCode method to compute bash based on value semantic not identity";
    }
}
