﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class EqualityOperatorValueCheckAssertion : IdiomaticAssertion
    {
        private readonly IEqualityTestCaseProvider equalityTestCaseProvider;

        public EqualityOperatorValueCheckAssertion(IEqualityTestCaseProvider equalityTestCaseProvider)
        {
            this.equalityTestCaseProvider =
                equalityTestCaseProvider ?? throw new ArgumentNullException(nameof(equalityTestCaseProvider));
        }

        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo equalityOperator = type.GetEqualityOperatorMethod();

            foreach (EqualityTestCase testCase in equalityTestCaseProvider.For(type))
            {
                bool result =
                    (bool)equalityOperator.Invoke(null, new[] { testCase.FirstInstance, testCase.SecondInstance });

                if (result != testCase.ExpectedResult)
                {
                    if (testCase.ExpectedResult)
                    {
                        throw new EqualityOperatorPerformsIdentityCheckException(type);
                    }


                    throw new EqualityOperatorUnexpectedResultException(type, testCase);
                }
            }
        }
    }
}
