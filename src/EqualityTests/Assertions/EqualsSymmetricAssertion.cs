﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using AutoFixture.Kernel;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class EqualsSymmetricAssertion : IdiomaticAssertion
    {
        private readonly ISpecimenBuilder builder;

        public EqualsSymmetricAssertion(ISpecimenBuilder builder)
        {
            this.builder = builder ?? throw new ArgumentNullException(nameof(builder));
        }

        public override void Verify(MethodInfo methodInfo)
        {
            if (methodInfo is null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            if (methodInfo.ReflectedType is null && !methodInfo.IsObjectEqualsOverrideMethod())
            {
                return;
            }

            var type = methodInfo.ReflectedType;

            object firstInstance = builder.CreateInstanceOfType(type);
            object secondInstance = builder.CreateInstanceOfType(type);

            bool firstComparisonResult = firstInstance.Equals(secondInstance);
            bool secondComparisonResult = secondInstance.Equals(firstInstance);

            if (firstComparisonResult != secondComparisonResult)
            {
                throw new EqualsSymmetricException(type,
                    $"Equals implementation of type {type} is not symmetric. x.Equals(y) returns {firstComparisonResult} but y.Equals(x) return {secondComparisonResult}");
            }
        }
    }
}
