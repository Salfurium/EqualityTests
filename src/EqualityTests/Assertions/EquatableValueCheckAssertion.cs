﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class EquatableValueCheckAssertion : IdiomaticAssertion
    {
        private readonly IEqualityTestCaseProvider equalityTestCaseProvider;

        public EquatableValueCheckAssertion(IEqualityTestCaseProvider equalityTestCaseProvider)
        {
            this.equalityTestCaseProvider = equalityTestCaseProvider ?? throw new ArgumentNullException(nameof(equalityTestCaseProvider));
        }

        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo equalsFromIEquatable = type.GetStronglyTypedEqualsMethod();

            foreach (EqualityTestCase testCase in equalityTestCaseProvider.For(type))
            {
                bool result =
                    (bool)equalsFromIEquatable.Invoke(testCase.FirstInstance, new[] { testCase.SecondInstance });

                if (result != testCase.ExpectedResult)
                {
                    if (testCase.ExpectedResult)
                    {
                        throw new EquatablePerformsIdentityCheckException(type);
                    }

                    throw new EquatableValueCheckException(type, testCase);
                }
            }
        }
    }
}
