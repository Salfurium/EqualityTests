﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Extensions;
    using System;
    using System.Reflection;

    public class EqualsOverrideAssertion : IdiomaticAssertion
    {
        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo equalsMethod = type.GetEqualsMethod();

            if (equalsMethod.IsObjectEqualsMethod())
            {
                throw new EqualsOverrideException(
                    $"Expected type {type.Name} to override Equals method");
            }
        }
    }
}
