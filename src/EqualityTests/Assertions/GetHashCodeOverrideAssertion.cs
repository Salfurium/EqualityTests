﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Extensions;
    using System;
    using System.Reflection;

    public class GetHashCodeOverrideAssertion : IdiomaticAssertion
    {
        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo getHashCodeMethod = type.GetMethod("GetHashCode");

            if (getHashCodeMethod.IsObjectGetHashCodeMethod())
            {
                throw new GetHashCodeOverrideException($"Expected type {type.Name} to override GetHashCode method");
            }
        }
    }
}
