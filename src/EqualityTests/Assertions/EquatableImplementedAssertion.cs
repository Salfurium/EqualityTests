﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using System;

    public class EquatableImplementedAssertion : IdiomaticAssertion
    {
        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (!typeof(IEquatable<>).MakeGenericType(type).IsAssignableFrom(type))
            {
                throw new EquatableNotImplementedException(type);
            }
        }
    }
}
