﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using AutoFixture.Kernel;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class EqualsTransitiveAssertion : IdiomaticAssertion
    {
        private readonly ISpecimenBuilder builder;

        public EqualsTransitiveAssertion(ISpecimenBuilder builder)
        {
            this.builder = builder ?? throw new ArgumentNullException(nameof(builder));
        }

        public override void Verify(MethodInfo methodInfo)
        {
            if (methodInfo is null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            if (methodInfo.ReflectedType is null && !methodInfo.IsObjectEqualsOverrideMethod())
            {
                return;
            }

            RecordReplayConstructorSpecimensForTypeBuilder recordReplayBuilder =
                new RecordReplayConstructorSpecimensForTypeBuilder(builder,
                    new ExactTypeSpecification(methodInfo.ReflectedType));

            object firstInstance = recordReplayBuilder.CreateInstanceOfType(methodInfo.ReflectedType);
            object secondInstance = recordReplayBuilder.CreateInstanceOfType(methodInfo.ReflectedType);
            object thirdInstance = recordReplayBuilder.CreateInstanceOfType(methodInfo.ReflectedType);

            bool firstToSecondComparisonResult = firstInstance.Equals(secondInstance);
            bool secondToThirdComparisonResult = secondInstance.Equals(firstInstance);
            bool firstToThirdComparisonResult = firstInstance.Equals(thirdInstance);

            if ((firstToSecondComparisonResult && secondToThirdComparisonResult) != true)
            {
                throw new EqualsTransitiveException(
                    "Can't check transitive property of Equals implementation due to object created with the same values doesn't result true, probably they are performing identity check instead of value check");
            }

            if ((firstToSecondComparisonResult && secondToThirdComparisonResult) != firstToThirdComparisonResult)
            {
                throw new EqualsTransitiveException(
                    $"Equals implementation of type {methodInfo.ReflectedType} is not transitive. It breaks following rule x.Equals(y) && y.Equals(z) == true then x.Equals(z) == true");
            }
        }
    }
}
