﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class EqualityOperatorOverloadAssertion : IdiomaticAssertion
    {
        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo equalityOperatorOverload = type.GetEqualityOperatorMethod();

            if (equalityOperatorOverload is null)
            {
                throw new EqualityOperatorNotOverloadedException(type);
            }
        }
    }
}
