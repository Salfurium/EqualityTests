﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using System;

    public class EqualsValueCheckAssertion : IdiomaticAssertion
    {
        private readonly IEqualityTestCaseProvider equalityTestCaseProvider;

        public EqualsValueCheckAssertion(IEqualityTestCaseProvider equalityTestCaseProvider)
        {
            this.equalityTestCaseProvider = equalityTestCaseProvider ??
                                            throw new ArgumentNullException(nameof(equalityTestCaseProvider));
        }

        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            foreach (EqualityTestCase testCase in equalityTestCaseProvider.For(type))
            {
                bool result = testCase.FirstInstance.Equals(testCase.SecondInstance);

                if (result != testCase.ExpectedResult)
                {
                    if (testCase.ExpectedResult)
                    {
                        throw new EqualsValueBasedOnIdentityException(type);
                    }

                    throw new EqualsValueUnexpectedResultException(type, testCase);
                }
            }
        }
    }
}
