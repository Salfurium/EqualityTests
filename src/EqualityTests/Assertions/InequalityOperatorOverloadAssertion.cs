﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using Extensions;
    using System;
    using System.Reflection;

    public class InequalityOperatorOverloadAssertion : IdiomaticAssertion
    {
        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            MethodInfo inequalityOperatorOverload = type.GetInequalityOperatorMethod();

            if (inequalityOperatorOverload == null)
            {
                throw new InequalityOperatorException(
                    $"Expected type {type.Name} to overload != operator with parameters of type {type.Name}");
            }
        }
    }
}
