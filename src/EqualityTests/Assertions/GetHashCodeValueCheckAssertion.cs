﻿namespace EqualityTests.Assertions
{
    using AutoFixture.Idioms;
    using Exceptions;
    using System;

    public class GetHashCodeValueCheckAssertion : IdiomaticAssertion
    {
        private readonly IEqualityTestCaseProvider equalityTestCaseProvider;

        public GetHashCodeValueCheckAssertion(IEqualityTestCaseProvider equalityTestCaseProvider)
        {
            this.equalityTestCaseProvider = equalityTestCaseProvider ??
                                            throw new ArgumentNullException(nameof(equalityTestCaseProvider));
        }

        public override void Verify(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            foreach (EqualityTestCase testCase in equalityTestCaseProvider.For(type))
            {
                int firstInstanceHashCode = testCase.FirstInstance.GetHashCode();
                int secondInstanceHashCode = testCase.SecondInstance.GetHashCode();
                bool result = firstInstanceHashCode == secondInstanceHashCode;

                if (result != testCase.ExpectedResult)
                {
                    if (testCase.ExpectedResult)
                    {
                        throw new GetHashCodeIdentityException(type);
                    }

                    throw new GetHashCodeValueCheckException(type, testCase);
                }
            }
        }
    }
}
