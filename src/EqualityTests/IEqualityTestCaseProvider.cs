﻿namespace EqualityTests
{
    using System;
    using System.Collections.Generic;

    public interface IEqualityTestCaseProvider
    {
        IEnumerable<EqualityTestCase> For(Type type);
    }
}
