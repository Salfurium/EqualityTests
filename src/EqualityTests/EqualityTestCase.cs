namespace EqualityTests
{
    using System;

    public class EqualityTestCase
    {
        public EqualityTestCase(object firstInstance, object secondInstance, bool expectedResult)
        {
            FirstInstance = firstInstance ?? throw new ArgumentNullException(nameof(firstInstance));
            SecondInstance = secondInstance ?? throw new ArgumentNullException(nameof(secondInstance));
            ExpectedResult = expectedResult;
        }

        public object FirstInstance { get; }
        public object SecondInstance { get; }
        public bool ExpectedResult { get; }
    }
}
