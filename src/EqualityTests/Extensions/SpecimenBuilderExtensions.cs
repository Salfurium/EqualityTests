﻿namespace EqualityTests.Extensions
{
    using AutoFixture.Kernel;
    using System;

    public static class SpecimenBuilderExtensions
    {
        public static object CreateInstanceOfType(this ISpecimenBuilder builder, Type type)
        {
            return builder.Create(type, new SpecimenContext(builder));
        }
    }
}
