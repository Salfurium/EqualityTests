namespace EqualityTests
{
    using AutoFixture.Kernel;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class EqualityTestCaseProvider : IEqualityTestCaseProvider
    {
        private readonly ISpecimenBuilder specimenBuilder;

        public EqualityTestCaseProvider(ISpecimenBuilder specimenBuilder)
        {
            this.specimenBuilder = specimenBuilder ?? throw new ArgumentNullException(nameof(specimenBuilder));
        }

        public IEnumerable<EqualityTestCase> For(Type type)
        {
            ConstructorArgumentsTracker tracker =
                new ConstructorArgumentsTracker(specimenBuilder, type.GetConstructors().Single());

            object instance = tracker.CreateNewInstance();
            object anotherInstance = tracker.CreateNewInstanceWithTheSameCtorArgsAsIn(instance);

            yield return new EqualityTestCase(instance, anotherInstance, true);

            foreach (object distinctInstance in tracker.CreateDistinctInstancesByChangingOneByOneCtorArgIn(instance))
            {
                yield return new EqualityTestCase(instance, distinctInstance, false);
            }
        }
    }
}
