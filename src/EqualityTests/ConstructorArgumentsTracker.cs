﻿namespace EqualityTests
{
    using AutoFixture.Kernel;
    using Extensions;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class ConstructorArgumentsTracker
    {
        private readonly SpecimensUsedInConstructorCollector collector;
        private readonly ConstructorInfo constructorInfo;
        private readonly ISpecimenBuilder specimenBuilder;

        public ConstructorArgumentsTracker(ISpecimenBuilder specimenBuilder, ConstructorInfo constructorInfo)
        {
            this.constructorInfo = constructorInfo ?? throw new ArgumentNullException(nameof(constructorInfo));
            this.specimenBuilder = specimenBuilder ?? throw new ArgumentNullException(nameof(specimenBuilder));
            collector = new SpecimensUsedInConstructorCollector();
        }

        public object CreateNewInstance()
        {
            List<object> parameters = (from pi in constructorInfo.GetParameters()
                select specimenBuilder.CreateInstanceOfType(pi.ParameterType)).ToList();

            object instance = constructorInfo.Invoke(parameters.ToArray());

            collector.AddSpecimens(instance, parameters.ToArray());

            return instance;
        }

        public object CreateNewInstanceWithTheSameCtorArgsAsIn(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return constructorInfo.Invoke(collector.GetSpecimens(obj));
        }

        public IEnumerable<object> CreateDistinctInstancesByChangingOneByOneCtorArgIn(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return DistinctInstancesFor(obj);
        }

        private IEnumerable<object> DistinctInstancesFor(object obj)
        {
            object[] arguments = collector.GetSpecimens(obj);

            for (int idx = 0; idx < arguments.Length; idx++)
            {
                yield return constructorInfo.Invoke(arguments.Select(
                    (o, i) => i == idx ? specimenBuilder.CreateInstanceOfType(o.GetType()) : o).ToArray());
            }
        }
    }
}
