﻿namespace EqualityTests.UnitTests
{
    using AutoFixture;
    using AutoFixture.AutoNSubstitute;
    using AutoFixture.Xunit2;

    public class AutoDomainData : AutoDataAttribute
    {
        public AutoDomainData() : base(FixtureFactory)
        {
        }

        private static IFixture FixtureFactory()
        {
            return new Fixture().Customize(new AutoNSubstituteCustomization());
        }
    }
}
