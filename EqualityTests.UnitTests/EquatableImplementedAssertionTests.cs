﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class EquatableImplementedAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(EquatableImplementedAssertion sut)
        {
            Assert.IsAssignableFrom<EquatableImplementedAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldVerifyMethodGuardCheckArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(EquatableImplementedAssertion).GetMethod("Verify",
                new[] { typeof(Type) }));
        }
    }
}
