﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class EqualsOverrideAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(EqualsOverrideAssertion sut)
        {
            Assert.IsAssignableFrom<IdiomaticAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckVerifyMethodArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(EqualsOverrideAssertion).GetMethod("Verify", new[] { typeof(Type) }));
        }
    }
}
