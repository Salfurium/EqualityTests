﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class EqualityOperatorOverloadAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(EqualityOperatorOverloadAssertion sut)
        {
            Assert.IsAssignableFrom<IdiomaticAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckVerifyMethodArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(EqualityOperatorOverloadAssertion).GetMethod("Verify",
                new[] { typeof(Type) }));
        }
    }
}
