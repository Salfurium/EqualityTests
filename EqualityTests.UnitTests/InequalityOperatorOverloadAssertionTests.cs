﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class InequalityOperatorOverloadAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(InequalityOperatorOverloadAssertion sut)
        {
            Assert.IsAssignableFrom<IdiomaticAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckVerifyMethodArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(
                typeof(InequalityOperatorOverloadAssertion).GetMethod("Verify", new[] { typeof(Type) }));
        }
    }
}
