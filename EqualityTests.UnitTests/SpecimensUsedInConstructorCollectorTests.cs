﻿namespace EqualityTests.UnitTests
{
    using System;
    using Xunit;

    public class SpecimensUsedInConstructorCollectorTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldThrowWhenPassedInstanceWasNotCollected(
            SpecimensUsedInConstructorCollector sut)
        {
            Exception exception = Record.Exception(() => sut.GetSpecimens(new object()));

            Assert.IsType<InvalidOperationException>(exception);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldExplainWhyCannotCreateInstanceWhichWasNotTrackedByTracker(
            SpecimensUsedInConstructorCollector sut)
        {
            object instance = new object();
            Exception exception = Record.Exception(() => sut.GetSpecimens(instance));

            Assert.Equal(string.Format("Collector does not contain specimens for instance {0}", instance),
                exception.Message);
        }
    }
}
