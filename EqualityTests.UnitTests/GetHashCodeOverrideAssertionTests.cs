﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class GetHashCodeOverrideAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(GetHashCodeOverrideAssertion sut)
        {
            Assert.IsAssignableFrom<IdiomaticAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckVerifyMethodArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(
                typeof(GetHashCodeOverrideAssertion).GetMethod("Verify", new[] { typeof(Type) }));
        }
    }
}
