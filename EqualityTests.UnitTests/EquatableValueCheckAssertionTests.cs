﻿namespace EqualityTests.UnitTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public class EquatableValueCheckAssertionTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldBeIdiomaticAssertion(EquatableValueCheckAssertion sut)
        {
            Assert.IsAssignableFrom<IdiomaticAssertion>(sut);
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckConstructorArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(EquatableValueCheckAssertion).GetConstructors());
        }

        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckVerifyMethodArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(
                typeof(EquatableValueCheckAssertion).GetMethod("Verify", new[] { typeof(Type) }));
        }
    }
}
