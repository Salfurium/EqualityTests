﻿namespace EqualityTests.UnitTests
{
    using AutoFixture.AutoNSubstitute;
    using AutoFixture.Idioms;
    using AutoFixture.Kernel;
    using NSubstitute;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class ConstructorArgumentsTrackerTests
    {
        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckConstructorArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(ConstructorArgumentsTracker).GetConstructors().Single());
        }
    }

    public class ConstructorArgumentsTrackerTests_CreateNewInstanceMethod
    {
        [Theory]
        [AutoDomainData]
        public void ShouldCreateNewInstanceCreateParameters(
            [Substitute] ISpecimenBuilder specimenBuilder)
        {
            ConstructorArgumentsTracker sut = new ConstructorArgumentsTracker(specimenBuilder, typeof(SimpleType)
                .GetConstructors().Single());

            sut.CreateNewInstance();

            specimenBuilder.Received(1).Create(Arg.Is(typeof(int)), Arg.Any<ISpecimenContext>());
        }
    }

    public class ConstructorArgumentsTrackerTests_CreateNewInstanceWithTheSameCtorArgsAsIn
    {
        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(ConstructorArgumentsTracker)
                .GetMethod(nameof(ConstructorArgumentsTracker.CreateNewInstanceWithTheSameCtorArgsAsIn)));
        }

        [Theory]
        [AutoDomainData]
        public void ShouldCreateInstanceWithTheSameCtorArgs(
            [Substitute] ISpecimenBuilder specimenBuilder)
        {
            ConstructorArgumentsTracker sut = new ConstructorArgumentsTracker(specimenBuilder,
                typeof(decimal).GetConstructor(new[] { typeof(double) }));

            object instance = sut.CreateNewInstance();
            object newInstance = sut.CreateNewInstanceWithTheSameCtorArgsAsIn(instance);

            Assert.False(ReferenceEquals(instance, newInstance));
            Assert.True(instance.Equals(newInstance));
        }
    }

    public class ConstructorArgumentTrackerTests_CreateDistinctInstancesByChaningOneByOneCtorArgInMethod
    {
        [Theory]
        [AutoDomainData]
        public void ShouldGuardCheckArguments(GuardClauseAssertion guardClauseAssertion)
        {
            guardClauseAssertion.Verify(typeof(ConstructorArgumentsTracker)
                .GetMethod(nameof(ConstructorArgumentsTracker.CreateDistinctInstancesByChangingOneByOneCtorArgIn)));
        }

        [Theory]
        [AutoDomainData]
        public void ShouldCreateAsManyDistinctInstancesAsCtorParameters(
            [Substitute] ISpecimenBuilder specimenBuilder)
        {
            specimenBuilder.Create(Arg.Any<object>(), Arg.Any<ISpecimenContext>()).Returns(1);

            ConstructorArgumentsTracker sut =
                new ConstructorArgumentsTracker(specimenBuilder, typeof(SimpleType).GetConstructors().Single());

            SimpleType instance = sut.CreateNewInstance() as SimpleType;
            specimenBuilder.ClearReceivedCalls();

            List<object> instances = sut.CreateDistinctInstancesByChangingOneByOneCtorArgIn(instance).ToList();

            specimenBuilder.Received(1).Create(Arg.Is(typeof(int)), Arg.Any<ISpecimenContext>());
            Assert.Single(instances);
        }
    }
}
