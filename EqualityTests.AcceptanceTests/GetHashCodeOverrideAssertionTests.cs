﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using AutoFixture.Xunit2;
    using System;
    using Xunit;

    public class GetHashCodeOverrideAssertionTests
    {
        [Theory]
        [AutoData]
        public void ShouldNotThrowWhenTypeOverridesGetHashCodeMethod(GetHashCodeOverrideAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(ClassWhichOverrideGetHashCodeMethod)));

            Assert.Null(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowWhenTypeDoesNotOverrideGetHashCodeMethod(GetHashCodeOverrideAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(ClassWhichDoesNotOverrideGetHashCodeMethod)));

            Assert.IsType<GetHashCodeOverrideException>(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldContainTypeNameInExceptionMessage(GetHashCodeOverrideAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(ClassWhichDoesNotOverrideGetHashCodeMethod)));

            Assert.Contains(typeof(ClassWhichDoesNotOverrideGetHashCodeMethod).Name, exception.Message);
        }

        public class ClassWhichOverrideGetHashCodeMethod
        {
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public class ClassWhichDoesNotOverrideGetHashCodeMethod
        {
        }
    }
}
