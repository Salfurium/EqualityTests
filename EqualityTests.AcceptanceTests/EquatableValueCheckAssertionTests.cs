﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using Exceptions;
    using System;
    using Xunit;

    public class EquatableValueCheckAssertionTests
    {
        [Theory]
        [AutoTestData]
        public void ShouldThrowWhenIEquatableEqualsPerformsIdentityCheck(EquatableValueCheckAssertion sut)
        {
            EqualityTestAssert.ExceptionWasThrownForTestType<EquatablePerformsIdentityCheckException, IEquatableWithIdentityCheck>
                (sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldExplainWhyExceptionIsThrownWhenIEquatablePerformsIdentityCheck(
            EquatableValueCheckAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(IEquatableWithIdentityCheck)));

            Assert.Contains(
                $"Expected IEquatable<{nameof(IEquatableWithIdentityCheck)}>.Equals method to perform value check but looks like it performs identity check", exception.Message);
        }

        [Theory]
        [AutoTestData]
        public void ShouldNotThrowWhenIEquatablePerformsValueCheck(EquatableValueCheckAssertion sut)
        {
            EqualityTestAssert.ExceptionWasNotThrownForTestType<IEquatableWithValueCheck>(sut);
        }
    }

    public class IEquatableWithValueCheck : IEquatable<IEquatableWithValueCheck>
    {
        public IEquatableWithValueCheck(int x)
        {
            X = x;
        }

        public int X { get; }

        public bool Equals(IEquatableWithValueCheck other)
        {
            return X == other.X;
        }
    }

    public class IEquatableWithIdentityCheck : IEquatable<IEquatableWithIdentityCheck>
    {
        public bool Equals(IEquatableWithIdentityCheck other)
        {
            return ReferenceEquals(this, other);
        }
    }
}
