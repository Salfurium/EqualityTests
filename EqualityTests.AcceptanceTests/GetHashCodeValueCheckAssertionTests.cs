﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using Exceptions;
    using System;
    using Xunit;

    public class GetHashCodeValueCheckAssertionTests
    {
        [Theory]
        [AutoTestData]
        public void ShouldNotThrowWhenGetHashCodeProducesHashBasedOnValues(GetHashCodeValueCheckAssertion sut)
        {
            EqualityTestAssert.ExceptionWasNotThrownForTestType<ValueObjectHashCodeExample>(sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldThrowWhenGetHashCodeProducesHashBasedOnIdentity(GetHashCodeValueCheckAssertion sut)
        {
            EqualityTestAssert
                .ExceptionWasThrownForTestType<GetHashCodeIdentityException, IdentityObjectHashCodeExample>(sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldExplainWhyExceptionIsThrownWhenHashCodeIsProducedOnIdentity(
            GetHashCodeValueCheckAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(IdentityObjectHashCodeExample)));

            Assert.Contains(
                string.Format(
                    "Expected type {0} GetHashCode method to compute bash based on value semantic not identity",
                    typeof(IdentityObjectHashCodeExample).Name), exception.Message);
        }
    }

    public class IdentityObjectHashCodeExample
    {
    }

    public class ValueObjectHashCodeExample
    {
        public ValueObjectHashCodeExample(int x)
        {
            X = x;
        }

        public int X { get; }

        public override int GetHashCode()
        {
            return X.GetHashCode();
        }
    }
}
