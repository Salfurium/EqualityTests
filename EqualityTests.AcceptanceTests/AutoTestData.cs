﻿namespace EqualityTests.AcceptanceTests
{
    using AutoFixture;
    using AutoFixture.Xunit2;
    using Customizations;

    public class AutoTestData : AutoDataAttribute
    {
        public AutoTestData() : base(FixtureFactory)
        {
        }

        private static IFixture FixtureFactory()
        {
            return new Fixture().Customize(new EqualityTestCaseProviderCustomization());
        }
    }
}
