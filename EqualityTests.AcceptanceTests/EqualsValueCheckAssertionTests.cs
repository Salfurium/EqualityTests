﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using Exceptions;
    using System;
    using Xunit;

    public class EqualsValueCheckAssertionTests
    {
        [Theory]
        [AutoTestData]
        public void ShouldThrowWhenIdentityCheckInEqualsImplementation(EqualsValueCheckAssertion sut)
        {
            EqualityTestAssert.ExceptionWasThrownForTestType<EqualsValueBasedOnIdentityException, object>(sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldExplainWhyExceptionIsThrownWhenEqualsIsIdentityCheck(EqualsValueCheckAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(Object)));

            Assert.Equal(
                $"Expected type {nameof(Object)} to perform value check but looks like it performs identity check",
                exception.Message);
        }

        [Theory]
        [AutoTestData]
        public void ShouldNotThrowWhenValueCheckInEqualsImplementation(EqualsValueCheckAssertion sut)
        {
            EqualityTestAssert.ExceptionWasNotThrownForTestType<ValueObjectExample>(sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldThrowWhenNotEveryCtorArgumentInfluenceEquality(EqualsValueCheckAssertion sut)
        {
            EqualityTestAssert
                .ExceptionWasThrownForTestType
                    <EqualsValueUnexpectedResultException, ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl>(sut);
        }

        [Theory]
        [AutoTestData]
        public void ShouldExplainWhyExceptionIsThrownWhenCtorArgDoesNotInfluenceEquality(EqualsValueCheckAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl)));

            Assert.Equal(
                $"Expected {new ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl(1, 1)} to be not equal to {new ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl(1, 1)}", exception.Message);
        }

        public class ValueObjectExample
        {
            public ValueObjectExample(int x)
            {
                X = x;
            }

            public int X { get; }

            public override bool Equals(object obj)
            {
                ValueObjectExample vo = obj as ValueObjectExample;

                return X == vo.X;
            }
        }

        public class ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl
        {
            public ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl(int x, int y)
            {
                X = x;
            }

            public int X { get; }

            public override bool Equals(object obj)
            {
                ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl vo =
                    obj as ValueObjectButSecondCtrArgDoesntTakePartInEqualsImpl;

                return X == vo.X;
            }
        }

        public class ValueObjectWhichTakesOtherValueObjectInCtor
        {
            public ValueObjectWhichTakesOtherValueObjectInCtor(ValueObjectExample valueObject)
            {
                ValueObject = valueObject;
            }

            public ValueObjectExample ValueObject { get; }

            public override bool Equals(object obj)
            {
                ValueObjectWhichTakesOtherValueObjectInCtor vo = obj as ValueObjectWhichTakesOtherValueObjectInCtor;

                return ValueObject.Equals(vo.ValueObject);
            }
        }
    }
}
