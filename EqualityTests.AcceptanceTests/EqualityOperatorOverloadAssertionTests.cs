﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using AutoFixture.Xunit2;
    using Exceptions;
    using System;
    using Xunit;

    public class EqualityOperatorOverloadAssertionTests
    {
        [Theory]
        [AutoData]
        public void ShouldNotThrowWhenEqualityOperatorIsOverloaded(EqualityOperatorOverloadAssertion sut)
        {
            EqualityTestAssert.ExceptionWasNotThrownForTestType<ClassWhichOverloadsEqualityOperator>(sut);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowWhenEqualityOperatorIsNotOverloaded(EqualityOperatorOverloadAssertion sut)
        {
            EqualityTestAssert
                .ExceptionWasThrownForTestType<EqualityOperatorNotOverloadedException, ClassWhichDoesNotOverloadsEqualityOperator>(
                    sut);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowWhenEqualityOperatorIsOverloadedWithArgumentsOtherThanContainingType(
            EqualityOperatorOverloadAssertion sut)
        {
            EqualityTestAssert.ExceptionWasThrownForTestType
            <EqualityOperatorNotOverloadedException,
                ClassThatOverloadsEqualityOperatorWithArgumentsOtherThanContainingType>(sut);
        }

        [Theory]
        [AutoData]
        public void ShouldContainTypeInExceptionMessage(EqualityOperatorOverloadAssertion sut)
        {
            Exception exception =
                Record.Exception(() => sut.Verify(typeof(ClassWhichDoesNotOverloadsEqualityOperator)));

            Assert.Contains(nameof(ClassWhichDoesNotOverloadsEqualityOperator), exception.Message);
        }

        [Theory]
        [AutoData]
        public void ShouldContainParametersTypeInExceptionMessage(EqualityOperatorOverloadAssertion sut)
        {
            Exception exception =
                Record.Exception(() => sut.Verify(typeof(ClassWhichDoesNotOverloadsEqualityOperator)));

            Assert.Contains(
                string.Format("with parameters of type {0}", nameof(ClassWhichDoesNotOverloadsEqualityOperator)),
                exception.Message);
        }

        public class ClassWhichOverloadsEqualityOperator
        {
            public static bool operator ==(ClassWhichOverloadsEqualityOperator a, ClassWhichOverloadsEqualityOperator b)
            {
                return true;
            }

            public static bool operator !=(ClassWhichOverloadsEqualityOperator a, ClassWhichOverloadsEqualityOperator b)
            {
                return true;
            }
        }

        public class ClassWhichDoesNotOverloadsEqualityOperator
        {
        }

        public class ClassThatOverloadsEqualityOperatorWithArgumentsOtherThanContainingType
        {
            public static bool operator ==(ClassThatOverloadsEqualityOperatorWithArgumentsOtherThanContainingType a,
                string b)
            {
                return true;
            }

            public static bool operator !=(ClassThatOverloadsEqualityOperatorWithArgumentsOtherThanContainingType a,
                string b)
            {
                return true;
            }
        }
    }
}
