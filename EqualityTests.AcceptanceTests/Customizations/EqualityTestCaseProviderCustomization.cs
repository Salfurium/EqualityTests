﻿namespace EqualityTests.AcceptanceTests.Customizations
{
    using AutoFixture;
    using AutoFixture.Kernel;

    public class EqualityTestCaseProviderCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<IEqualityTestCaseProvider>(
                composer => composer.FromFactory(() =>
                    new EqualityTestCaseProvider(fixture.Create<ISpecimenBuilder>())));
        }
    }
}
