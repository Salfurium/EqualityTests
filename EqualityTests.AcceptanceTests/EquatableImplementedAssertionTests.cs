﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using AutoFixture.Xunit2;
    using Exceptions;
    using System;
    using Xunit;

    public class EquatableImplementedAssertionTests
    {
        [Theory]
        [AutoData]
        public void ShouldNotThrowExceptionWhenIEquatableIsImplemented(EquatableImplementedAssertion sut)
        {
            EqualityTestAssert.ExceptionWasNotThrownForTestType<ClassThatImplementsIEquatable>(sut);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowExceptionWhenIEquatableIsNotImplemented(EquatableImplementedAssertion sut)
        {
            EqualityTestAssert
                .ExceptionWasThrownForTestType<EquatableNotImplementedException,
                    ClassWhichDoesNotImplementIEquatable>(sut);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowExceptionWhenIEquatableIsImplementedWithoutTypeThatImplementsInterface(
            EquatableImplementedAssertion sut)
        {
            EqualityTestAssert
                .ExceptionWasThrownForTestType
                    <EquatableNotImplementedException, ClassThatImplementsIEquatableWithOtherType>(sut);
        }

        [Theory]
        [AutoData]
        public void ShouldExceptionMessageContainTypeName(EquatableImplementedAssertion sut)
        {
            Exception exception = Record.Exception(() => sut.Verify(typeof(ClassWhichDoesNotImplementIEquatable)));

            Assert.Contains(typeof(ClassWhichDoesNotImplementIEquatable).Name, exception.Message);
        }

        public class ClassThatImplementsIEquatable : IEquatable<ClassThatImplementsIEquatable>
        {
            public bool Equals(ClassThatImplementsIEquatable other)
            {
                throw new NotImplementedException();
            }
        }

        public class ClassWhichDoesNotImplementIEquatable
        {
        }

        public class ClassThatImplementsIEquatableWithOtherType : IEquatable<string>
        {
            public bool Equals(string other)
            {
                throw new NotImplementedException();
            }
        }
    }
}
