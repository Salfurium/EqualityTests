﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using AutoFixture.Idioms;
    using AutoFixture.Xunit2;
    using System;
    using Xunit;

    public class EqualsOverrideAssertionTests
    {
        [Theory]
        [AutoData]
        public void ShouldThrowWhenClassDoesNotOverloadEqualsMethod(EqualsOverrideAssertion sut)
        {
            Exception exception = Record.Exception(() => sut.Verify(typeof(ClassWhichDoesNotOverrideEqualsMethod)));

            Assert.IsType<EqualsOverrideException>(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldNotThrowWhenClassOverloadEqualsMethod(EqualsOverrideAssertion sut)
        {
            Exception exception = Record.Exception(() => sut.Verify(typeof(ClassWhichOverridesEqualsMethod)));

            Assert.Null(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldExceptionMessageContainTypeName(EqualsOverrideAssertion sut)
        {
            Exception exception = Record.Exception(() => sut.Verify(typeof(ClassWhichDoesNotOverrideEqualsMethod)));

            Assert.Contains(nameof(ClassWhichDoesNotOverrideEqualsMethod), exception.Message);
        }

        public class ClassWhichDoesNotOverrideEqualsMethod
        {
        }

        public class ClassWhichOverridesEqualsMethod
        {
            public override bool Equals(object obj)
            {
                // ReSharper disable once BaseObjectEqualsIsObjectEquals
                return base.Equals(obj);
            }
        }
    }
}
