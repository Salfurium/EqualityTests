﻿namespace EqualityTests.AcceptanceTests
{
    using AutoFixture.Idioms;
    using System;
    using Xunit;

    public static class EqualityTestAssert
    {
        public static void ExceptionWasNotThrownForTestType<T>(IdiomaticAssertion idiomaticAssertion)
        {
            Exception exception = Record.Exception(
                () => idiomaticAssertion.Verify(typeof(T)));

            Assert.Null(exception);
        }

        public static void ExceptionWasThrownForTestType<TException, TTestType>(IdiomaticAssertion sut)
        {
            Exception exception = Record.Exception(
                () => sut.Verify(typeof(TTestType)));

            Assert.IsType<TException>(exception);
        }
    }
}
