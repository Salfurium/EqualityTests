﻿namespace EqualityTests.AcceptanceTests
{
    using Assertions;
    using AutoFixture.Xunit2;
    using Exceptions;
    using System;
    using System.Reflection;
    using System.Threading;
    using Xunit;

    public class EqualsSymmetricTests
    {
        [Theory]
        [AutoData]
        public void ShouldNotThrowWhenEqualsIsIdentityCheckAndIsSymmetric(EqualsSymmetricAssertion sut)
        {
            Exception exception =
                Record.Exception(
                    () => sut.Verify(typeof(object).GetMethod("Equals", BindingFlags.Public | BindingFlags.Instance)));
            Assert.Null(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldNotThrowWhenValueObjectEqualsImplementationIsSymmetric(EqualsSymmetricAssertion sut)
        {
            Exception exception =
                Record.Exception(
                    () =>
                        sut.Verify(typeof(ValueObjectSymmetricEqualsImplementationExample).GetMethod("Equals",
                            BindingFlags.Public | BindingFlags.Instance)));
            Assert.Null(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldThrowWhenEqualsImplementationIsNotSymmetric(EqualsSymmetricAssertion sut)
        {
            Exception exception =
                Record.Exception(
                    () =>
                        sut.Verify(typeof(NotSymmetricEqualsImplementationExample).GetMethod("Equals",
                            BindingFlags.Public | BindingFlags.Instance)));

            Assert.IsType<EqualsSymmetricException>(exception);
        }

        [Theory]
        [AutoData]
        public void ShouldExceptionMessageContainTypeName(EqualsSymmetricAssertion sut)
        {
            Exception exception =
                Record.Exception(
                    () =>
                        sut.Verify(typeof(NotSymmetricEqualsImplementationExample).GetMethod("Equals",
                            BindingFlags.Public | BindingFlags.Instance)));

            Assert.Contains(nameof(NotSymmetricEqualsImplementationExample), exception.Message);
        }

        public class NotSymmetricEqualsImplementationExample
        {
            private static int _typeCounter = -1;
            private readonly int instanceCounter;

            public NotSymmetricEqualsImplementationExample()
            {
                instanceCounter = Interlocked.Increment(ref _typeCounter);
            }

            public override bool Equals(object obj)
            {
                return instanceCounter % 2 == 0;
            }
        }

        public class ValueObjectSymmetricEqualsImplementationExample
        {
            private readonly int x;

            public ValueObjectSymmetricEqualsImplementationExample(int x)
            {
                this.x = x;
            }

            public override bool Equals(object obj)
            {
                ValueObjectSymmetricEqualsImplementationExample other =
                    obj as ValueObjectSymmetricEqualsImplementationExample;

                return x == other.x;
            }
        }
    }
}
